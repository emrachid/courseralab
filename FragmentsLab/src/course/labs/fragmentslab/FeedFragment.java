package course.labs.fragmentslab;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FeedFragment extends Fragment {

	private static final String TAG = "Lab-Fragments";
	private static final String LAST_ITEM_SELECTED = "last_item_selected";

	private TextView mTextView;
	private int mPosition = -1;
	private static FeedFragmentData feedFragmentData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Log.i("euler",
				"FeedFragment - onCreateView called - savedInstanceState = "
						+ savedInstanceState);

		return inflater.inflate(R.layout.feed, container, false);

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.i("euler", "FeedFragment - onCreate called - savedInstanceState = "
				+ savedInstanceState);

		setRetainInstance(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Log.i("euler",
				"FeedFragment - onActivityCreated called - savedInstanceState = "
						+ savedInstanceState);

		// Read in all Twitter feeds
		if (null == feedFragmentData) {

			feedFragmentData = new FeedFragmentData(getActivity());

		}
	}

	// Display Twitter feed for selected feed

	void updateFeedDisplay(int position) {

		Log.i(TAG, "Entered updateFeedDisplay()");

		mPosition = position;

		mTextView = (TextView) getView().findViewById(R.id.feed_view);
		mTextView.setText(feedFragmentData.getFeed(position));

	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		Log.i("euler", "FeedFragment - onAttach called");
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		Log.i("euler", "FeedFragment - onConfigurationChanged called");
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i("euler", "FeedFragment - onDestroy called");
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		Log.i("euler", "FeedFragment - onDestroyView called");
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		Log.i("euler", "FeedFragment - onDetach called");
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.i("euler", "FeedFragment - onPause called");
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.i("euler", "FeedFragment - onResume called");
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		Log.i("euler", "FeedFragment - onSaveInstanceState called");

		outState.putInt(LAST_ITEM_SELECTED, mPosition);
		Log.i("euler", "mPostion = " + mPosition);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Log.i("euler", "FeedFragment - onStart called");

		Log.i("euler", "mPostion = " + mPosition);
		if (mPosition != -1) {
			updateFeedDisplay(mPosition);
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.i("euler", "FeedFragment - onStop called");
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		Log.i("euler",
				"FeedFragment - onViewCreated called - savedInstanceState = "
						+ savedInstanceState);
	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewStateRestored(savedInstanceState);
		Log.i("euler",
				"FeedFragment - onViewStateRestored called - savedInstanceState = "
						+ savedInstanceState);

		if (savedInstanceState != null) {
			mPosition = savedInstanceState.getInt(LAST_ITEM_SELECTED, -1);
		}
		Log.i("euler", "mPostion = " + mPosition);
	}

}
