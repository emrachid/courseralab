package course.labs.fragmentslab;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity implements
		FriendsFragment.SelectionListener {

	private static final String TAG = "Lab-Fragments";

	private FriendsFragment mFriendsFragment;
	private FeedFragment mFeedFragment;

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		Log.i("euler", "finish called");
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Log.i("euler", "onBackPressed called");
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		Log.i("euler", "onSaveInstanceState called");

		Log.i("euler", "isChangingConfigurations " + isChangingConfigurations());

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);

		Log.i("euler", "onCreate called");

		Log.i("euler", "isChangingConfigurations " + isChangingConfigurations());

		// If the layout is single-pane, create the FriendsFragment
		// and add it to the Activity

		if (!isInTwoPaneMode()) {

			Log.i("euler", "isInTwoPaneMode");
			
			mFriendsFragment = (FriendsFragment) getFragmentManager()
					.findFragmentByTag("friendFragTag");

			Log.i("euler", "onCreate - mFriendsFragment = " + mFriendsFragment);

			if (mFriendsFragment == null) {
				// if ((mFriendsFragment == null) && (mPosition == -1)) {
				mFriendsFragment = new FriendsFragment();

				FragmentManager fragmentManager = getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager
						.beginTransaction();
				fragmentTransaction.add(R.id.fragment_container,
						mFriendsFragment, "friendFragTag");
				fragmentTransaction.commit();
			}

			mFeedFragment = (FeedFragment) getFragmentManager()
					.findFragmentByTag("FragmentFeedTag");
			Log.i("euler", "onCreate - mFeedFragment = " + mFeedFragment);

			if (mFeedFragment == null) {
				Log.i("euler", "onCreate - New FeedFragment()");
				mFeedFragment = new FeedFragment();
			}

		} else {
			// Otherwise, save a reference to the FeedFragment for later use
			mFeedFragment = (FeedFragment) getFragmentManager()
					.findFragmentById(R.id.feed_frag);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.i("euler", "onPause called");
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Log.i("euler", "onStart called");

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.i("euler", "onStop called");
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		Log.i("euler", "onResume called");
	}

	// If there is no fragment_container ID, then the application is in
	// two-pane mode
	private boolean isInTwoPaneMode() {

		return findViewById(R.id.fragment_container) == null;

	}

	// Display selected Twitter feed

	public void onItemSelected(int position) {

		Log.i(TAG, "Entered onItemSelected(" + position + ")");

		// If in single-pane mode, replace single visible Fragment

		if (!isInTwoPaneMode()) {

			FragmentManager fragmentManager = getFragmentManager();
			// fragmentManager.popBackStackImmediate();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			fragmentTransaction.replace(R.id.fragment_container, mFeedFragment,
					"FragmentFeedTag");
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();

			// execute transaction now
			getFragmentManager().executePendingTransactions();

		}

		// Update Twitter feed display on FriendFragment
		mFeedFragment.updateFeedDisplay(position);

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		Log.i("euler", "onRestart called");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i("euler", "onDestroy called");
	}

}
