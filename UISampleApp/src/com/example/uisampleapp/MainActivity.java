package com.example.uisampleapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class MainActivity extends FragmentActivity {

	private static final String TAG = "MainActivity";

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.action_settings:
			showSettings();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void showSettings() {
		Log.d(TAG, "Creating settings...");
		Intent intent = new Intent(this, SettingsActivity.class);
		startActivity(intent);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new SectionFragment();
			Bundle args = new Bundle();
			args.putInt(SectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 8 total pages.
			return 8;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			case 3:
				return getString(R.string.title_section4).toUpperCase(l);
			case 4:
				return getString(R.string.title_section5).toUpperCase(l);
			case 5:
				return getString(R.string.title_section6).toUpperCase(l);
			case 6:
				return getString(R.string.title_section7).toUpperCase(l);
			case 7:
				return getString(R.string.title_section8).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class SectionFragment extends ListFragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public SectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {

			List<Match> matchList = null;

			int sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
			switch (sectionNumber) {
			case 1:
				matchList = showGroupA();
				break;
			case 2:
				matchList = showGroupB();
				break;
			case 3:
				matchList = showGroupC();
				break;
			case 4:
				matchList = showGroupD();
				break;
			case 5:
				matchList = showGroupE();
				break;
			case 6:
				matchList = showGroupF();
				break;
			case 7:
				matchList = showGroupG();
				break;
			case 8:
				matchList = showGroupH();
				break;
			default:
				// log error it should never happen
				break;
			}

			GroupAdapter adapter = new GroupAdapter(inflater.getContext(),
					R.layout.match_item, matchList);

			setListAdapter(adapter);

			return super.onCreateView(inflater, container, savedInstanceState);
		}

		private List<Match> showGroupA() {
			List<Match> matchList = new ArrayList<Match>();
			matchList.add(new Match(R.string.brazil, R.string.croatia,
					"12/Jun, 17:00 - Arena Corinthias, S�o Paulo"));
			matchList.add(new Match(R.string.mexico, R.string.cameroon,
					"13/Jun, 13:00 - Arena das Dunas, Natal"));
			matchList.add(new Match(R.string.brazil, R.string.mexico,
					"17/Jun, 16:00 - Castel�o, Fortaleza"));
			matchList.add(new Match(R.string.cameroon, R.string.mexico,
					"18/Jun, 19:00 - Arena Amaz�nia, Manaus"));
			matchList.add(new Match(R.string.cameroon, R.string.brazil,
					"23/Jun, 17:00 - Man� Garrincha, Bras�lia"));
			matchList.add(new Match(R.string.croatia, R.string.mexico,
					"23/Jun, 17:00 - Arena Pernambuco, Recife"));

			return matchList;
		}

		private List<Match> showGroupB() {
			List<Match> matchList = new ArrayList<Match>();
			matchList.add(new Match(R.string.spain, R.string.netherlands,
					"13/Jun, 16:00 - Fonte Nova, Salvador"));
			matchList.add(new Match(R.string.chile, R.string.australia,
					"13/Jun, 19:00 - Arena Pantanal, Cuiab�"));
			matchList.add(new Match(R.string.spain, R.string.chile,
					"18/Jun, 19:00 - Arena Pantanal, Cuiab�"));
			matchList.add(new Match(R.string.australia, R.string.netherlands,
					"18/Jun, 16:00 - Maracan�, Rio de Janeiro"));
			matchList.add(new Match(R.string.australia, R.string.spain,
					"23/Jun, 13:00 - Arena da Baixada, Curitiba"));
			matchList.add(new Match(R.string.netherlands, R.string.chile,
					"23/Jun, 13:00 - Arena Corinthias, S�o Paulo"));

			return matchList;
		}

		private List<Match> showGroupC() {
			List<Match> matchList = new ArrayList<Match>();
			matchList.add(new Match(R.string.colombia, R.string.greece,
					"14/Jun, 13:00 - Mineir�o, Belo Horizonte"));
			matchList.add(new Match(R.string.ivory_coast, R.string.japan,
					"14/Jun, 22:00 - Arena Pernambuco, Recife"));
			matchList.add(new Match(R.string.colombia, R.string.ivory_coast,
					"19/Jun, 13:00 - Man� Garrincha, Bras�lia"));
			matchList.add(new Match(R.string.japan, R.string.greece,
					"19/Jun, 19:00 - Arena das Dunas, Natal"));
			matchList.add(new Match(R.string.japan, R.string.colombia,
					"24/Jun, 17:00 - Arena Pantanal, Cuiab�"));
			matchList.add(new Match(R.string.greece, R.string.ivory_coast,
					"24/Jun, 17:00 - Castel�o, Fortaleza"));

			return matchList;
		}

		private List<Match> showGroupD() {
			List<Match> matchList = new ArrayList<Match>();
			matchList.add(new Match(R.string.uruguai, R.string.costa_rica,
					"14/Jun, 16:00 - Castel�o, Fortaleza"));
			matchList.add(new Match(R.string.england, R.string.italy,
					"14/Jun, 19:00 - Arena Amaz�nia, Manaus"));
			matchList.add(new Match(R.string.uruguai, R.string.england,
					"19/Jun, 16:00 - Arena Corinthias, S�o Paulo"));
			matchList.add(new Match(R.string.italy, R.string.costa_rica,
					"20/Jun, 13:00 - Arena Pernambuco, Recife"));
			matchList.add(new Match(R.string.italy, R.string.uruguai,
					"24/Jun, 13:00 - Arena das Dunas, Natal"));
			matchList.add(new Match(R.string.costa_rica, R.string.england,
					"24/Jun, 13:00 - Mineir�o, Belo Horizonte"));

			return matchList;
		}

		private List<Match> showGroupE() {
			List<Match> matchList = new ArrayList<Match>();
			matchList.add(new Match(R.string.switzerland, R.string.ecuador,
					"15/Jun, 13:00 - Man� Garrincha, Bras�lia"));
			matchList.add(new Match(R.string.france, R.string.honduras,
					"15/Jun, 16:00 - Beira-Rio, Porto Alegre"));
			matchList.add(new Match(R.string.switzerland, R.string.france,
					"20/Jun, 16:00 - Fonte Nova, Salvador"));
			matchList.add(new Match(R.string.honduras, R.string.ecuador,
					"20/Jun, 19:00 - Arena da Baixada, Curitiba"));
			matchList.add(new Match(R.string.honduras, R.string.switzerland,
					"25/Jun, 17:00 - Arena Amaz�nia, Manaus"));
			matchList.add(new Match(R.string.ecuador, R.string.france,
					"25/Jun, 17:00 - Maracan�, Rio de Janeiro"));

			return matchList;
		}

		private List<Match> showGroupF() {
			List<Match> matchList = new ArrayList<Match>();
			matchList.add(new Match(R.string.argentina, R.string.bosnia,
					"15/Jun, 19:00 - Maracan�, Rio de Janeiro"));
			matchList.add(new Match(R.string.iran, R.string.nigeria,
					"16/Jun, 16:00 - Arena da Baixada, Curitiba"));
			matchList.add(new Match(R.string.argentina, R.string.iran,
					"21/Jun, 13:00 - Mineir�o, Belo Horizonte"));
			matchList.add(new Match(R.string.nigeria, R.string.bosnia,
					"21/Jun, 19:00 - Arena do Pantanal, Cuiab�"));
			matchList.add(new Match(R.string.nigeria, R.string.argentina,
					"25/Jun, 13:00 - Beira-Rio, Porto Alegre"));
			matchList.add(new Match(R.string.bosnia, R.string.iran,
					"25/Jun, 13:00 - Fonte Nova, Salvador"));

			return matchList;
		}

		private List<Match> showGroupG() {
			List<Match> matchList = new ArrayList<Match>();
			matchList.add(new Match(R.string.germany, R.string.portugal,
					"16/Jun, 13:00 - Fonte Nova, Salvador"));
			matchList.add(new Match(R.string.ghana, R.string.united_states,
					"16/Jun, 19:00 - Arena das Dunas, Natal"));
			matchList.add(new Match(R.string.germany, R.string.ghana,
					"21/Jun, 16:00 - Castel�o, Fortaleza"));
			matchList.add(new Match(R.string.united_states, R.string.portugal,
					"22/Jun, 19:00 - Arena Amaz�nia, Manaus"));
			matchList.add(new Match(R.string.united_states, R.string.germany,
					"26/Jun, 13:00 - Arena Pernambuco, Recife"));
			matchList.add(new Match(R.string.portugal, R.string.ghana,
					"26/Jun, 13:00 - Man� Garrincha, Bras�lia"));

			return matchList;
		}

		private List<Match> showGroupH() {
			List<Match> matchList = new ArrayList<Match>();
			matchList.add(new Match(R.string.belgium, R.string.algeria,
					"17/Jun, 13:00 - Mineir�o, Belo Horizonte"));
			matchList.add(new Match(R.string.russia, R.string.south_korea,
					"17/Jun, 19:00 - Arena Pantanal, Cuiab�"));
			matchList.add(new Match(R.string.belgium, R.string.russia,
					"22/Jun, 13:00 - Maracan�, Rio de Janeiro"));
			matchList.add(new Match(R.string.south_korea, R.string.algeria,
					"22/Jun, 16:00 - Beira-Rio, Porto Alegre"));
			matchList.add(new Match(R.string.south_korea, R.string.belgium,
					"26/Jun, 17:00 - Arena Corinthias, S�o Paulo"));
			matchList.add(new Match(R.string.algeria, R.string.russia,
					"26/Jun, 17:00 - Arena da Baixada, Curitiba"));

			return matchList;
		}

	}

}
