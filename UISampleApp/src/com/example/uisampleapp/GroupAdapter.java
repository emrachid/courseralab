package com.example.uisampleapp;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class GroupAdapter extends ArrayAdapter<Match> {
	private List<Match> mItems;

	public GroupAdapter(Context context, int resource, List<Match> objects) {
		super(context, resource, objects);

		mItems = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.match_item, null);
		}

		// get item to be displayed in the actual position
		Match match = mItems.get(position);

		if (match != null) {
			TextView teamAView = (TextView) convertView
					.findViewById(R.id.team_a);
			teamAView.setText(match.getTeamA());

			TextView teamBView = (TextView) convertView
					.findViewById(R.id.team_b);
			teamBView.setText(match.getTeamB());

			TextView timePlace = (TextView) convertView
					.findViewById(R.id.match_place_time);
			timePlace.setText(match.getTimePlace());
		}

		return convertView;
	}

}
