package com.example.uisampleapp;

public class Match {
	private int mTeamA;
	private int mScoreA;
	private int mTeamB;
	private int mScoreB;
	private String mTimePlace;

	Match(int teamA, int teamB, String timePlace) {
		setTeamA(teamA);
		setTeamB(teamB);
		setTimePlace(timePlace);
	}

	Match(int teamA, int scoreA, int scoreB, int teamB, String timePlace) {
		setTeamA(teamA);
		setTeamB(teamB);
		setScoreA(scoreA);
		setScoreB(scoreB);
		setTimePlace(timePlace);
	}

	public int getTeamA() {
		return mTeamA;
	}

	public void setTeamA(int teamA) {
		this.mTeamA = teamA;
	}

	public int getScoreA() {
		return mScoreA;
	}

	public void setScoreA(int scoreA) {
		this.mScoreA = scoreA;
	}

	public int getTeamB() {
		return mTeamB;
	}

	public void setTeamB(int teamB) {
		this.mTeamB = teamB;
	}

	public int getScoreB() {
		return mScoreB;
	}

	public void setScoreB(int scoreB) {
		this.mScoreB = scoreB;
	}

	public String getTimePlace() {
		return mTimePlace;
	}

	public void setTimePlace(String timePlace) {
		this.mTimePlace = timePlace;
	}
}
