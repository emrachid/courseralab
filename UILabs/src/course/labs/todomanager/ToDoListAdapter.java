package course.labs.todomanager;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import course.labs.todomanager.ToDoItem.Status;

public class ToDoListAdapter extends BaseAdapter implements OnClickListener {

	// List of ToDoItems
	private final List<ToDoItem> mItems = new ArrayList<ToDoItem>();

	private SparseBooleanArray mCheckedItens = new SparseBooleanArray();

	private final Context mContext;

	private static final String TAG = "Lab-UserInterface";

	private class ViewHolder {

		public TextView titleView;
		public CheckBox statusView;
		public TextView priorityView;
		public TextView dateView;
	}

	public ToDoListAdapter(Context context) {

		mContext = context;

	}

	// Add a ToDoItem to the adapter
	// Notify observers that the data set has changed

	public void add(ToDoItem item) {

		mCheckedItens.put(mItems.size(), item.getStatus() == Status.DONE);
		mItems.add(item);
		notifyDataSetChanged();

	}

	// Clears the list adapter of all items.

	public void clear() {

		mItems.clear();
		notifyDataSetChanged();

	}

	// Returns the number of ToDoItems

	@Override
	public int getCount() {

		return mItems.size();

	}

	// Retrieve the number of ToDoItems

	@Override
	public Object getItem(int pos) {

		return mItems.get(pos);

	}

	// Get the ID for the ToDoItem
	// In this case it's just the position

	@Override
	public long getItemId(int pos) {

		return pos;

	}

	// Create a View to display the ToDoItem
	// at specified position in mItems

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final ToDoItem toDoItem = (ToDoItem) getItem(position);
		final ViewHolder viewHolder;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.todo_item, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.titleView = (TextView) convertView
					.findViewById(R.id.titleView);

			viewHolder.statusView = (CheckBox) convertView
					.findViewById(R.id.statusCheckBox);
			viewHolder.statusView.setOnClickListener(this);

			viewHolder.priorityView = (TextView) convertView
					.findViewById(R.id.priorityView);
			viewHolder.dateView = (TextView) convertView
					.findViewById(R.id.dateView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.titleView.setText(toDoItem.getTitle());

		viewHolder.statusView.setChecked(mCheckedItens.get(position, false));
		viewHolder.statusView.setTag(Integer.valueOf(position));

		viewHolder.priorityView.setText(toDoItem.getPriority().toString());

		viewHolder.dateView.setText(ToDoItem.FORMAT.format(toDoItem.getDate()));

		// Return the View you just created
		return convertView;

	}

	private void log(String msg) {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Log.i(TAG, msg);
	}

	@Override
	public void onClick(View checkBoxView) {
		// get the item position
		Integer position = (Integer) checkBoxView.getTag();
		if (position != null) {
			boolean isChecked = ((CheckBox) checkBoxView).isChecked();
			ToDoItem checkedItem = (ToDoItem) getItem(position);
			checkedItem.setStatus(isChecked ? Status.DONE : Status.NOTDONE);
			mCheckedItens.put(position, isChecked);
		}
	}
}
